<?php
/**
 * Plugin Name:       Protect MyWP Files
 * Plugin URI:        https://www.whodunit.fr/
 * Description:       Private files. 🔒
 * Version:           1.0.0
 * Author:            Agence Whodunit
 * Author URI:        https://www.whodunit.fr/
 * License:           GPLv2
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       Protect-MyWP-Files
 * Domain Path:       /languages
 */

namespace Protect_MyWP_Files;

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

define( 'MYWP_FILES_VERSION', '1.0.0' );
define( 'MYWP_FILES_PATH', __DIR__ );
define( 'MYWP_FILES_FILE', __FILE__ );

require_once MYWP_FILES_PATH . '/class/translation.php';


/**
 * Admin
 */

if ( is_admin() ) {
	require_once MYWP_FILES_PATH . '/upgrade/upgrade.php';
}


require_once MYWP_FILES_PATH . '/class/tools.php';

require_once MYWP_FILES_PATH . '/class/register-plugin.php';


/**
 * Mise en place du CPT
 */
require_once MYWP_FILES_PATH . '/class/post_type.php';

/**
 * Création de la sous page
 */
require_once MYWP_FILES_PATH . '/class/subpage.php';

require_once MYWP_FILES_PATH . '/class/enqueue.php';

require_once MYWP_FILES_PATH . '/class/ajax.php';

require_once MYWP_FILES_PATH . '/class/rewrite.php';
require_once MYWP_FILES_PATH . '/class/download.php';
