<?php

use Protect_MyWP_Files\Tools;


defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

if ( isset( $_POST ) && count( $_POST ) > 0 && isset( $_POST[ 'submit-nginx' ] ) ) {
	if ( wp_verify_nonce( $_POST[ 'nonce' ], 'captain_submit_nginx' ) ) {
		if ( isset( $_POST[ 'captain-secure-nginx' ] ) && $_POST[ 'captain-secure-nginx' ] == 1 ) {
			Tools\captain_sart_plugin( 1 );
		} else {
			Tools\captain_sart_plugin( 0 );
		}
	}
}

$start_plugin = get_option( 'captain-start-plugin', 0 );

$error_plugin = 0;

$path_secure   = apply_filters( 'captain_secure_folder', 'protect-files' );
$uploads_dir   = Tools\get_folder_uploads() . '/' . $path_secure;
$rewrite_share = apply_filters( 'captain_secure_rewrite_share', 'private-download' );
$url_front     = get_home_url() . '/' . $rewrite_share;

$folderperms = substr( sprintf( '%o', fileperms( Tools\get_folder_uploads() ) ), - 4 );

if ( $folderperms == '0755' || $folderperms == '0750' ) {
	$folder_create = Tools\check_folder_create();

	$perms = substr( sprintf( '%o', fileperms( $uploads_dir ) ), - 4 );

	if ( $perms != '0755' && $perms != '0750' ) {
		$error_plugin ++;
		Tools\captain_start_plugin( 0 );
		$folder_create = sprintf( 'Directory "%s" was not created. Please do it manually for the proper functioning of the extension. The folder can have the rights 755 or 750.', $uploads_dir );
	}
} else {
	$error_plugin ++;
	$folder_create = sprintf(
	/* translators: %s full paths */
		esc_html__( 'The folder "%s" does not have the right rights. Please make the change: 0755 or 0750', 'Protect-MyWP-Files' ),
		Tools\get_folder_uploads()
	);
}

$env = Tools\check_env();

if ( $env == 'apache' ) {
	$check_htaccess = Tools\check_htaccess_create();
	if ( ! $check_htaccess ) {
		$error_plugin ++;
	}
}

if ( $env == 'nginx' && $start_plugin == 0 ) {
	$error_plugin ++;
}

$success = '<span class="dashicons dashicons-yes-alt"></span>';
$error   = '<span class="dashicons dashicons-dismiss"></span>';
$warning = '<span class="dashicons dashicons-warning"></span>';

?>

<style>
    .captain-language-markup {
        background: #000;
        color: #fff;
        padding: 20px;
    }

    .captain-success {
        color: #00a32a;
    }

    .captain-error {
        color: #c03;
    }

    .captain-warning {
        color: #dba617;
    }

    .captain-success.captain-success-info, .captain-error.captain-error-info {
        background: #000;
        padding: 20px;
        text-align: center;
        font-size: 30px;
    }

    .captain-success.captain-success-info .dashicons, .captain-error.captain-error-info .dashicons {
        font-size: 50px;
    }
</style>
<div class="wrap">
	<h2><?php _e( 'Settings', 'Protect-MyWP-Files' ) ?></h2>
	<table class="form-table">
		<tbody>
		<tr>
			<th scope="row"><?php _e( 'Path in which private media are saved.', 'Protect-MyWP-Files' ) ?></th>
			<td>
				<?php if ( $folder_create != '' ): ?>
					<p class="captain-error">
						<?php echo $error ?><?php echo $folder_create; ?>
					</p>
				<?php else : ?>
					<p class="captain-success">
						<?php echo $success ?><?php _e( 'The folder has been created', 'Protect-MyWP-Files' ) ?>
					</p>
				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e( 'Share link', 'Protect-MyWP-Files' ) ?></th>
			<td>
				<p>
					<strong><?php echo $url_front ?>/xxxxx</strong>
				</p>
			</td>
		</tr>
		<?php if ( $env == 'nginx' ) :
			$html = __( 'Thank you for protecting the folder', 'Protect-MyWP-Files' );
			$code_secure = 'location ~* ^/wp-content/uploads/' . $path_secure . '/.*.$ {
	deny all;
}';
			?>
			<tr>
				<th scope="row"><?php _e( 'Protection of folder', 'Protect-MyWP-Files' ) ?></th>
				<td>
					<p class="captain-warning">
						<?php echo $warning ?><?php echo $html ?>
					</p>
					<p class="captain-error">
					<pre class="captain-language-markup"><?php echo $code_secure ?></pre>
					</p>
					<form action="<?php menu_page_url( 'captain-media-secure-submenu-page-lib', true ) ?>" method="POST">
						<label for="captain-secure-nginx">
							<input <?php echo ( $start_plugin ) ? 'checked' : '' ?> name="captain-secure-nginx" type="checkbox" id="captain-secure-nginx" value="1">
							<input type="hidden" name="nonce" value="<?php echo wp_create_nonce( 'captain_submit_nginx' ) ?>">
							<?php _e( 'I take responsibility for protecting the folder.', 'Protect-MyWP-Files' ) ?>
						</label>
						<input type="submit" name="submit-nginx" class="button button-small button-primary" value="<?php _e( 'Update', 'Protect-MyWP-Files' ) ?>"/>
					</form>

				</td>
			</tr>
		<?php endif; ?>
		<?php if ( $env == 'apache' ) : ?>
			<tr>
				<th scope="row"><?php _e( 'Protection of folder', 'Protect-MyWP-Files' ) ?></th>
				<td>
					<?php if ( $check_htaccess ) : ?>
						<p class="captain-success">
							<?php echo $success ?><?php _e( 'The .htaccess file has been created', 'Protect-MyWP-Files' ) ?><br>
							<?php _e( 'I invite you to test this link to make sure that the file is properly protected.', 'Protect-MyWP-Files' ) ?>
							<?php if ( file_exists( $uploads_dir . '/captain-america-test-secure.png' ) ) : ?>
								<a href="<?php echo home_url() ?>/wp-content/uploads/<?php echo $path_secure ?>/captain-america-test-secure.png" target="_blank"><?php _e( 'Try me', 'Protect-MyWP-Files' ) ?></a>
							<?php endif; ?>
						</p>
					<?php else :
						$html = __( 'You must also create an .htaccess file with the rights "644", containing:', 'Protect-MyWP-Files' );
						$code_secure = 'deny from all';
						?>
						<p class="captain-warning">
							<?php echo $warning ?><?php echo $html ?>
						</p>
						<p class="captain-error">
						<pre class="captain-language-markup"><?php echo $code_secure ?></pre>
						</p>
					<?php endif; ?>
				</td>
			</tr>
		<?php endif; ?>


		</tbody>
	</table>

	<?php if ( $error_plugin == 0 ) : ?>

		<p class="captain-success captain-success-info">
			<?php _e( 'The necessary steps have been taken. The extension will work properly.', 'Protect-MyWP-Files' ) ?> <span class="dashicons dashicons-awards"></span>
		</p>

	<?php else : ?>

		<p class="captain-error captain-error-info">
			<?php _e( 'Please check the above items in order for the extension to work. Once corrected, simply refresh', 'Protect-MyWP-Files' ) ?> <span class="dashicons dashicons-megaphone"></span>
		</p>

	<?php endif; ?>


</div>
