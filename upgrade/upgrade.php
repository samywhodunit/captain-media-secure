<?php

namespace Protect_MyWP_Files\Upgrade;

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

define( 'URL_UPGRADE', 'https://extensions.whodunit.fr/' );


class Upgrade {
	protected $slug;
	protected $version;
	protected $name;

	protected static $instance = null;

	public function __construct() {

		$this->slug            = 'protect-mywp-files';
		$this->version         = MYWP_FILES_VERSION;
		$this->name            = 'Protect MyWP Files';
		$this->plugin_basename = plugin_basename( MYWP_FILES_FILE );


		add_filter( 'pre_set_site_transient_update_plugins', [ $this, 'check_update' ] );
		add_filter( 'plugins_api', [ $this, 'checked_api' ], 10, 3 );


	}

	private function get_remote() {
		if ( false == $remote = get_transient( $this->slug . '_update' ) ) {
			$remote = wp_remote_get( URL_UPGRADE . 'protect-mywp-files/upgrade.php', [
					'timeout' => 10,
					'headers' => [
						'Accept' => 'application/json'
					]
				]
			);

			if ( ! is_wp_error( $remote ) && isset( $remote[ 'response' ][ 'code' ] ) && $remote[ 'response' ][ 'code' ] == 200 && ! empty( $remote[ 'body' ] ) ) {
				set_transient( $this->slug . '_update', $remote, 43200 ); // 12 heures de cache
			}
		}

		return $remote;
	}

	private function format_remote( $remote, $_transient_data ) {
		$remote = json_decode( $remote[ 'body' ] );
		if ( $remote && version_compare( $this->version, $remote->version, '<' ) && version_compare( $remote->requires, get_bloginfo( 'version' ), '<' ) ) {
			$response                 = new \stdClass();
			$response->name           = $this->name;
			$response->slug           = $this->slug;
			$response->version        = $remote->version;
			$response->requires       = $remote->requires;
			$response->tested         = $remote->tested;
			$response->new_version    = $remote->version;
			$response->plugin         = $this->plugin_basename;
			$response->author         = '<a href="https://whodunit.fr">Whodunit & Kantari Samy</a>';
			$response->author_profile = 'https://profiles.wordpress.org/leprincenoir';
			$response->package        = $remote->download_url;
			$response->last_updated   = $remote->last_updated;
			$response->url            = URL_UPGRADE;
			$response->sections       = [
				'description'  => $remote->sections->description,
				'installation' => $remote->sections->installation,
				'changelog'    => $remote->sections->changelog,
			];
			if ( ! empty( $remote->screenshots ) ) {
				$response->sections[ 'screenshots' ] = $remote->screenshots;
			}

			$response->banners = [
				'low'  => URL_UPGRADE . 'protect-mywp-files/assets/banner-772x250.jpg',
				'high' => URL_UPGRADE . 'protect-mywp-files/assets/banner-1544x500.jpg'
			];

			$_transient_data->response[ $this->plugin_basename ] = $response;


			$_transient_data->last_checked                      = current_time( 'timestamp' );
			$_transient_data->checked[ $this->plugin_basename ] = $this->version;

		}

		return $_transient_data;
	}

	public function check_update( $_transient_data ) {

		if ( ! is_object( $_transient_data ) ) {
			$_transient_data = new stdClass;
		}

		if ( ! empty( $_transient_data->response ) && ! empty( $_transient_data->response[ $this->plugin_basename ] ) ) {
			return $_transient_data;
		}

		$_transient_data = $this->format_remote( $this->get_remote(), $_transient_data );

		return $_transient_data;
	}

	public function who_dashboard_push_update( $transient ) {

		if ( empty( $transient->checked ) ) {
			return $transient;
		}


		$transient = $this->format_remote( $this->get_remote(), $transient );


		return $transient;
	}

	public function checked_api( $res, $action, $args ) {

		if ( $action != 'plugin_information' ) {
			return $res;
		}

		if ( ! isset( $args->slug ) || ( $args->slug != $this->slug ) ) {

			return $res;

		}


		$res = $this->format_remote( $this->get_remote(), $res );

		if ( isset( $res ) && isset( $res->response ) ) {
			$res = $res->response[ $this->plugin_basename ];
		}

		return $res;
	}

	public static function get_instance() {

		if ( null === self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
}

Upgrade::get_instance();
