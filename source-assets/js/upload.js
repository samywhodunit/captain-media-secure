function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}


function readURL(input) {
    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {
            jQuery('.file-upload-image').attr('src', e.target.result);
            jQuery('.file-upload-content').show();

            jQuery('.file-title').html(input.files[0].name + ' (' + formatBytes(input.files[0].size) + ')');
        };

        reader.readAsDataURL(input.files[0]);

    } else {
        removeUpload();
    }
}

function copyCaptain(item) {
    var id = item.parents('tr').attr('id');
    /* Get the text field */
    var copyText = document.querySelector('#' + id + ' .share-link textarea');

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /* For mobile devices */

    /* Copy the text inside the text field */
    document.execCommand("copy");
    jQuery('#copied-success').fadeIn(800);
    jQuery('#copied-success').fadeOut(800);
    /* Alert the copied text */
    //alert("Copied the text: " + copyText.value);
}

function removeUpload() {
    jQuery('.file-upload-content').hide();
    jQuery('.file-upload-input').val('');
    jQuery('.file-upload-wrap').removeClass('image-dropping');

}

jQuery(document).ready(function ($) {
    $('body').append('<div id="copied-success" class="copied"><span>Copied!</span></div>');
    $(".wrap h1").append('<a href="#captain-secure-popin" class="add-new-h2">' + CAPTAIN_INFO.i18n.add_media + '</a>');
    $('<div class="captain-media-secure-wrapper"><div class="file-upload"><div class="file-upload-wrap"><form id="captain-form-upload"><input id="protect-media" name="upload_captain" class="file-upload-input" type="file" /><input id="start" class="page-title-action" value="' + CAPTAIN_INFO.i18n.start_upload + '" type="submit" /></form><div class="drag-text"><p>' + CAPTAIN_INFO.i18n.dragdrop + '<br><button class="button">' + CAPTAIN_INFO.i18n.select_file + '</button></p></div></div><div class="file-upload-content"><div class="file-title-wrap"> <button type="button" onclick="removeUpload()" class="remove-file">' + CAPTAIN_INFO.i18n.remove + ' <span class="file-title">' + CAPTAIN_INFO.i18n.upload_file + '</span> <span class="dashicons dashicons-dismiss"></span></button></div></div></div><div class="errorUpload"></div><div class="captain-media-secure-result"><div class="captain-notice"></div></div><div class="captain-info">' + CAPTAIN_INFO.max_upload_format + '</div></div>').insertAfter(".wrap h1");

    $('.captain-media-secure-wrapper .drag-text .button').on('click', function () {
        $('#protect-media').click();
    });
    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than {0}');
    $("#captain-form-upload").validate({
        rules: {
            upload_captain: {
                required: true,
                extension: CAPTAIN_INFO.allowTypes,
                filesize: CAPTAIN_INFO.max_upload
            }
        },
        errorElement: 'div',
        errorLabelContainer: '.captain-notice',
        messages: {
            upload_captain: {
                extension: CAPTAIN_INFO.i18n.invalid_filetype,
                filesize: CAPTAIN_INFO.i18n.file_exceeds_size_limit,
            }
        },
        submitHandler: function (form) {
            captainAjax();
        }
    });


    $('#protect-media').on('change', function(){
      readURL(this);
    });

    $('.captain-copy .copy-clipboard').on('click', function (e) {
        e.preventDefault();
        copyCaptain($(this));
    });

    $(document).on('click', '.add-new-h2', function (e) {

        e.preventDefault();
        $('.captain-media-secure-wrapper').show();
    });


    $('.file-upload-wrap').bind('dragover', function () {
        $('.file-upload-wrap').addClass('image-dropping');
    });
    $('.file-upload-wrap').bind('dragleave', function () {
        $('.file-upload-wrap').removeClass('image-dropping');
    });


    function captainAjax() {
        var formData = new FormData();
        var file = document.getElementById('protect-media').files[0];
        formData.append('file', file);
        formData.append('nonce', CAPTAIN_INFO.nonce);
        formData.append('action', 'captain_upload_secure_file');

        $.ajax({
            url: CAPTAIN_INFO.ajax,
            type: "POST",
            cache: false,
            contentType: false,
            processType: false,
            processData: false,
            data: formData
        }).done(function (response) {
            var tag_result = $('.captain-media-secure-result');

            var tag_class = 'error';
            if (response.error === 0) {
                tag_class = 'success';
            } else {
                //  tag_result.find('#captain-media-secure-result-share-url').hide();
                //  tag_result.find('#captain-media-secure-result-share-url').val('');

            }
            removeUpload();

            tag_result.find('.captain-notice').html('<div class="' + tag_class + '">' + response.text + '</div>');
            tag_result.find('.captain-notice').show();

            setTimeout(function () {
                location.reload();
            }, 1000); 

        }).fail(function (response) {
            removeUpload(); /*
            var tag_result = $('.captain-media-secure-result');
            tag_result.removeClass('captain-secure-media-success');
            tag_result.addClass('captain-secure-media-error');
            tag_result.find('.captain-notice-success').show();
            tag_result.find('.captain-notice-success').text('');
            tag_result.find('#captain-media-secure-result-share-url').val('');
            tag_result.find('.captain-notice-success').text(response.responseText); */
        });

    };
});
