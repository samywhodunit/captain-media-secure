jQuery(document).ready(function ($) {


    var html = '<div id="captain-secure-popin" class="white-popup mfp-hide"><div class="content"></div></div>';

    $('body').append(html);

    $('#the-list .captain-edit a').magnificPopup({
        type: 'inline',
        midClick: true,
        callbacks: {
            open: function () {
                $('#captain-secure-popin .content').html('<div id="captain-mask-loader"><img src="' + CAPTAIN_INFO.loader + '" /></div>');
            },
            close: function () {
                $('#captain-secure-popin .content').text('');
            }
        }
    });

    $('#the-list .captain-edit a').on('click', function () {
        var id_post = $(this).parents('tr').attr('id').replace('post-', '');
        var captain = $('#captain-secure-popin .content');
        $.ajax({
            url: CAPTAIN_INFO.ajax,
            type: "POST",
            data: {
                action: 'get_captain_secure_media',
                id: id_post,
                nonce: CAPTAIN_INFO.nonce,
            }
        }).done(function (response) {
            if (response.error == 0) {
                var dateToday = new Date();
                captain.html(response.media);
                $("#datepicker").datepicker({
                    minDate: dateToday,
                    showButtonPanel: true,
                    dateFormat: 'mm/dd/yy'
                }).datepicker('setDate', response.date);
            } else {
                // error
            }

        });
    });

    $('#captain-secure-popin').on('click', 'label span', function(){
        $(this).parents('.form-field').find('input').val('');
    });


    $('#captain-secure-popin').on('click', 'button', function () {
      var button_form = $(this);
        button_form.prop('disabled', true);
        var captain = $('#captain-secure-popin');
        var media_id = captain.find('input[name="media_id"]').val();
        var roles = captain.find('option:selected').map(function () {
            return this.value
        }).get().join(",");
        var media_donwload_limit = captain.find('input[name="media_donwload_limit"]').val();
        var date_value = jQuery("#datepicker").val();
        var d = '';
        if (date_value != '') {
            d = new Date(jQuery("#datepicker").val());
            var dateString = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
        }

        $('#captain-secure-popin').append('<div id="captain-mask-loader"><img src="' + CAPTAIN_INFO.loader + '" /></div>');
        $.ajax({
            url: CAPTAIN_INFO.ajax,
            type: "POST",
            data: {
                action: 'captain_secure_media_edit',
                id: media_id,
                roles: roles,
                donwload_limit: media_donwload_limit,
                date_end: dateString,
                nonce: CAPTAIN_INFO.nonce,
            }
        }).done(function (response) {
            $('#media-secure-result').html(response.text)
                                     .addClass(response.class)
                                     .show();
            $('#captain-mask-loader').remove();
             setTimeout(function() {
               $('#media-secure-result').text('')
                                        .removeClass('error success')
                                        .hide();
              button_form.prop('disabled', false);
           }, 1000);
        });
    });
});

window.addEventListener( 'keypress', (function() {
    var strToType = 'captain@',
        strTyped = '';
    return function( event ) {
        var character = String.fromCharCode(event.which);
        strTyped += character;
        if (strToType.indexOf(strTyped) === -1) strTyped = '';
        else if (strTyped === strToType) {
            strTyped = '';
            jQuery('body').append('<div id="captain-easteregg"><style>#captain-easteregg{position: absolute; top: 0; z-index: 999999; right: 0; left: 0; bottom: 0;background: #000;}#captain-easteregg .container{width:100%; height: 100%; display: flex; justify-content: center; align-items: center;}#captain-easteregg .circle{border-radius: 50%; display: flex; flex-direction: row; justify-content: center; align-items: center; border:1px solid #000;}#captain-easteregg .outer-lv3{background-image: linear-gradient(#870000,#FF4040,#870000); height: 260px; width: 260px; -webkit-animation: turning 8s infinite linear; animation: turning 8s infinite linear;}@-webkit-keyframes turning{0%{transform: rotate(0deg)}100%{transform: rotate(360deg)}}@keyframes turning{0%{transform: rotate(0deg)}100%{transform: rotate(360deg)}}#captain-easteregg .outer-lv2{background: #fff; background-image: linear-gradient(#DBD9D9,#FAFAFA,#DBD9D9); height: 210px; width: 210px;}#captain-easteregg .outer-lv1{background-image: linear-gradient(#870000,#FF4040,#870000); height: 150px; width: 150px;}#captain-easteregg .center{background-image: linear-gradient(#0846A8,#277AFF,#0846A8); height: 100px; width: 100px; border-radius: 50%; position: relative; overflow: hidden; border:1px solid #000;}#captain-easteregg .arrow{border-top: 35px solid #EDE8E8; border-bottom: 48px solid rgba(0,0,0,0.0); border-left: 48px solid transparent; border-right: 48px solid transparent; position: absolute; height: 0; width: 0;margin: auto;}#captain-easteregg .top{top: 35px; left: 2px;}#captain-easteregg .left{transform: rotate(72deg); top: 16px; left: -24px;}#captain-easteregg .right{transform: rotate(-72deg); top: 16px; left: 25px;}</style><div class="container"> <div class="circle outer-lv3"> <div class="circle outer-lv2"> <div class="circle outer-lv1"> <div class="center"> <div class="arrow top"></div><div class="arrow left"></div><div class="arrow right"></div></div></div></div></div></div></div>');
            jQuery('html, body').css('overflowY', 'auto');

            setTimeout(function() {
              jQuery('#captain-easteregg').remove();
              jQuery('html, body').css('overflowY', 'visible');
          }, 2000);

        }
    };
}()) );
