const path = require('path');
const glob = require('glob');

const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: ['./js/main.js'].concat(glob.sync('./scss/**/*.scss')),
    output: {
        filename: 'mywp-files.js',
        path: path.resolve(__dirname, '../js')
    },
    mode: 'none',
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '../css/mywp-files.css',
        }),
    ],
    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin({
            exclude: /node_modules/,
            terserOptions: {
                format: {
                    comments: false,
                },
            },
            extractComments: false,
        })],
    },
    externals: {
        "jquery": "jquery"
    }

}