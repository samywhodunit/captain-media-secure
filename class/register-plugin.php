<?php

namespace Protect_MyWP_Files\Register;

use Protect_MyWP_Files\Tools;

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

/**
 * Activate the plugin.
 */
register_activation_hook( MYWP_FILES_FILE, function () {
	set_transient( 'protect-mywp-file-notice', true, 5 );
} );


/**
 * Deactivation hook.
 */
register_deactivation_hook( MYWP_FILES_FILE, function () {

} );


add_action( 'admin_notices', function () {

	if ( get_transient( 'protect-mywp-file-notice' ) ) {
		$folder_create = Tools\check_folder_create();

		$serv = '';
		if ( Tools\check_env() == 'nginx' ) {
			$serv = 'nginx';
		} else {
			Tools\check_htaccess_create();
		}

		if ( $folder_create != '' || $serv == 'nginx' ) :
			?>
			<div class="notice-warning notice is-dismissible">
				<p><a href="<?php menu_page_url( 'protect-mywp-file-settings', true ) ?>"><?php _e( 'Several elements must be controlled in order for the extension to work properly', 'Protect-MyWP-Files' ) ?></a></p>
			</div>
		<?php
		endif;
		delete_transient( 'protect-mywp-file-notice' );
	}
} );
