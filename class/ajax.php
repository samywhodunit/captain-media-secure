<?php

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

use Protect_MyWP_Files\Tools;

add_action( 'wp_ajax_captain_secure_media_edit', function () {
	$return = [
		'text'  => __( 'Update completed', 'Protect-MyWP-Files' ),
		'class' => 'success',
	];
	$error  = 0;
	if ( isset( $_POST[ 'nonce' ] ) && wp_verify_nonce( $_POST[ 'nonce' ], 'captain_upload_secure_file' ) ) {

		if ( isset( $_POST[ 'id' ] ) && $_POST[ 'id' ] != '' ) {
			$media_id = (int) $_POST[ 'id' ];
			if ( $media_id > 0 ) {

				if ( isset( $_POST[ 'roles' ] ) && $_POST[ 'roles' ] != '' ) {
					$roles = esc_html( (string) $_POST[ 'roles' ] );
					update_post_meta( $media_id, 'captain_media_secure_roles', explode( ',', $roles ) );
					$return[ 'media' ] = 1;
				}

				$donwload_limit = '';
				if ( isset( $_POST[ 'donwload_limit' ] ) && $_POST[ 'donwload_limit' ] != '' ) {
					$donwload_limit = (int) $_POST[ 'donwload_limit' ];

					$return[ 'media' ] = 1;
				}
				update_post_meta( $media_id, 'captain_media_secure_download_limit', $donwload_limit );

				$update_date = '';
				if ( isset( $_POST[ 'date_end' ] ) && $_POST[ 'date_end' ] != '' && $_POST[ 'date_end' ] != 'NaN-NaN-NaN' ) {
					$date = explode( '-', $_POST[ 'date_end' ] ); // YY-mm-dd

					if ( checkdate( $date[ 1 ], $date[ 2 ], $date[ 0 ] ) ) { // mm dd YY
						$date = new \DateTime( $_POST[ 'date_end' ] );
						$update_date = $date->getTimestamp();
						
						$return[ 'media' ] = 1;
					} else {
						$error = 1;
					}
				}
				update_post_meta( $media_id, 'captain_media_secure_expiration_date', $update_date );
			}
		} else {
			$error = 1;
		}


	} else {
		$error = 1;
	}

	if ( $error > 0 ) {
		$return[ 'text' ]  = __( 'An error has occurred. Try again', 'Protect-MyWP-Files' );
		$return[ 'class' ] = 'error';
	}
	wp_send_json( $return );
	exit();
} );


add_action( 'wp_ajax_get_captain_secure_media', function () {
	$return = [
		'media' => [],
		'date'  => '',
		'error' => 0,
	];
	if ( isset( $_POST[ 'nonce' ] ) && wp_verify_nonce( $_POST[ 'nonce' ], 'captain_upload_secure_file' ) ) {

		$media       = get_post( (int) $_POST[ 'id' ] );
		$media_roles = Tools\get_role_media_secure( $media->ID );

		$download_count = (int) get_post_meta( $media->ID, 'captain_media_secure_donwload_count', true );

		global $wp_roles;
		$html = '<h2>' . $media->post_title . '</h2>';
		$html .= '<h3 title="' . __( 'Download done', 'Protect-MyWP-Files' ) . '" class="count-download">' . __( 'Download done:', 'Protect-MyWP-Files' ) . ' ' . $download_count . '</h3>';
		$html .= '<input name="media_id" value="' . $media->ID . '" type="hidden" />';

		$html .= '<div class="form-field">
		<label for="field" class="label--required">' . __( 'Select roles', 'Protect-MyWP-Files' ) . '</label>
		<section>
		<span class="dashicons dashicons-admin-users"></span>';

		$html .= '<select name="select_roles" id="select_roles" multiple>';
		$html .= '<option value="all">' . __( 'All', 'Protect-MyWP-Files' ) . '</option>';

		foreach ( $wp_roles->roles as $key => $r ) {
			$selected = '';
			if ( in_array( $key, $media_roles ) ) {
				$selected = 'selected="selected"';
			}
			$html .= '<option ' . $selected . ' value="' . $key . '">' . translate_user_role( $r[ 'name' ] ) . '</option>';
		}
		$html  .= '</select>';
		$html  .= '</section>
		</div>';
		$limit = get_post_meta( $media->ID, 'captain_media_secure_download_limit', true );

		$download_limit = $limit;
		if ( $limit == '' || $limit == - 1 ) {
			$download_limit = '';
		}


		$html .= '<div class="form-field">
				<label for="field" class="label--required">' . __( 'Download limit', 'Protect-MyWP-Files' ) . ' <span title="'.__('Reset','Protect-MyWP-Files').'">x</span></label>
				<section>
						<span class="dashicons dashicons-download"></span>
						<input name="media_donwload_limit" value="' . $download_limit . '" type="number" />
				</section>
		</div>';

		$html .= '<div class="form-field">
				<label for="field" class="label--required">' . __( 'Date limit', 'Protect-MyWP-Files' ) . ' <span title="'.__('Reset','Protect-MyWP-Files').'">x</span></label>
				<section>
						<span class="dashicons dashicons-calendar-alt"></span>
						<input id="datepicker" name="date_limit" type="text" class="form-control" />
				</section>
		</div>';

		$html .= '<div id="media-secure-result"></div>';

		$html .= '<button type="button" aria-disabled="false" aria-expanded="false" class="">' . __( 'Update', 'Protect-MyWP-Files' ) . '</button>';

		$return[ 'media' ] = $html;

		$date_limit = get_post_meta( $media->ID, 'captain_media_secure_expiration_date', true );

		if ( $date_limit == '' || $date_limit == 0 ) {
			$txt = '';
		} else {
			$txt = date_i18n( 'm/d/Y', $date_limit );
		}

		$return[ 'date' ] = $txt;

	}

	wp_send_json( $return );
	exit();
} );

add_action( 'wp_ajax_captain_upload_secure_file', function () {
	$return = [
		'text'  => [],
		'error' => 0,
	];

	if ( isset( $_POST[ 'nonce' ] ) && wp_verify_nonce( $_POST[ 'nonce' ], 'captain_upload_secure_file' ) ) {

		$attachment_id = 0;
		if ( isset( $_FILES[ 'file' ] ) && $_FILES[ 'file' ][ 'error' ] == 0 ) {

			$uploads_dir = Tools\get_folder_uploads() . '/' . apply_filters( 'captain_secure_folder', 'protect-files' );

			$allowTypes = get_allowed_mime_types();

			$path        = pathinfo( $_FILES[ "file" ][ "name" ] );
			$tmp         = preg_replace( '/.' . $path[ 'extension' ] . '$/', '', $_FILES[ "file" ][ "name" ] ); // Remove extension from filename
			$name_format = sanitize_title( $tmp );                                                              // Remove accents && Lower case && Remove special characters

			$name = $name_format . '.' . $path[ 'extension' ];


			if ( ! in_array( $_FILES[ 'file' ][ 'type' ], $allowTypes ) ) {
				$return[ 'text' ][] = __( 'Sorry, this file type is not permitted for security reasons.', 'Protect-MyWP-Files' );
				$return[ 'error' ]  = 1;
			}

			$max_upload_size = wp_max_upload_size();
			if ( ! $max_upload_size ) {
				$max_upload_size = 0;
			}

			if ( $_FILES[ 'file' ][ 'size' ] > $max_upload_size ) {
				$return[ 'text' ][] = $name . ' ' . __( 'exceeds the maximum upload size for this site.', 'Protect-MyWP-Files' );
				$return[ 'error' ]  = 1;
			}

			if ( $return[ 'error' ] === 0 ) {
				$name = $name_format . '-' . uniqid() . '.' . $path[ 'extension' ];

				if ( move_uploaded_file( $_FILES[ "file" ][ "tmp_name" ], $uploads_dir . '/' . $name ) ) {

					$my_post = [
						'post_title'  => wp_strip_all_tags( $name_format ),
						'post_type'   => 'protect-mywp-file',
						'post_status' => 'publish'
					];

					$attachment_id = wp_insert_post( $my_post );
				} else {
					$return[ 'text' ][] = __( 'Looks like something&#8217;s gone wrong. Wait a couple seconds, and then try again.', 'Protect-MyWP-Files' );
				}

			}
		} else {
			$return[ 'text' ][] = __( 'Looks like something&#8217;s gone wrong. Wait a couple seconds, and then try again.', 'Protect-MyWP-Files' );
		}


		if ( $attachment_id > 0 && is_numeric( $attachment_id ) ) {

			$token = apply_filters( 'captain_secure_system_token', wp_generate_uuid4() );

			update_post_meta( $attachment_id, 'captain_media_secure', true );
			update_post_meta( $attachment_id, 'captain_media_secure_id', $token );
			update_post_meta( $attachment_id, 'captain_media_secure_name', $name );
			update_post_meta( $attachment_id, 'captain_media_secure_download_limit', '-1' );
			update_post_meta( $attachment_id, 'captain_media_secure_expiration_date', 0 );
			update_post_meta( $attachment_id, 'captain_media_secure_donwload_count', 0 );

			$return[ 'text' ] = __( 'The media has been correctly protected.', 'Protect-MyWP-Files' );
		}

		if ( $return[ 'error' ] === 1 ) {
			$return[ 'text' ] = implode( '<br>', $return[ 'text' ] );
		}
	}


	wp_send_json( $return );
	exit();
} );
