<?php

namespace Protect_MyWP_Files\CPT;

use Protect_MyWP_Files\Tools;

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function remove_new_captain() {
	global $typenow, $current_screen;
	if ( isset( $typenow ) && $typenow == 'protect-mywp-file' && ( ! isset( $_GET[ 'action' ] ) || ( $_GET[ 'action' ] != 'trash' && $_GET[ 'action' ] != 'untrash' ) ) ) {
		if ( isset( $current_screen ) && $current_screen->post_type == 'protect-mywp-file' ) {
			wp_redirect( admin_url( 'edit.php?post_type=protect-mywp-file' ), 301 );
			exit;
		}
	}
}

add_action( 'load-post-new.php', __NAMESPACE__ . '\\remove_new_captain' );
add_action( 'load-post.php', __NAMESPACE__ . '\\remove_new_captain' );


add_action( 'wp_untrash_post_status', function ( $new_status, $post_id, $previous_status ) {
	$post_type = get_post_type( $post_id );
	if ( $post_type == 'protect-mywp-file' ) {
		return 'publish';
	}

	return $new_status;
}, 11, 3 );


/**
 * Création post type
 */
add_action( 'init', function () {

	$labels = [
		'name'           => __( 'Private Files', 'Protect-MyWP-Files' ),
		'singular_name'  => __( 'Private Files', 'Protect-MyWP-Files' ),
		'menu_name'      => __( 'Private Files', 'Protect-MyWP-Files' ),
		'name_admin_bar' => __( 'Private Files', 'Protect-MyWP-Files' ),

	];

	$args = [
		'labels'             => $labels,
		'public'             => false,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'capabilities'       => [
			'create_posts' => false,
		],
		'map_meta_cap'       => true,
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => 11,
		'supports'           => [ 'title', 'author' ],
		'show_in_rest'       => 0,
		'menu_icon'          => plugin_dir_url( MYWP_FILES_FILE ) . 'img/lock20.svg'
	];

	register_post_type( 'protect-mywp-file', $args );

} );

/**
 * Suppression des liens pour créer un post
 */
add_action( 'admin_menu', function () {
	if ( ! is_admin() ) {
		return;
	}
	global $submenu;
	unset( $submenu[ 'edit.php?post_type=protect-mywp-file' ][ 10 ] );

}, 999 );


add_action( 'admin_bar_menu', function ( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'new-protect-mywp-file' );

}, 999 );


/**
 * Ajout nouvelle colonne
 */
add_filter( 'manage_edit-protect-mywp-file_columns', function ( $columns ) {

	$columns = array_slice( $columns, 0, 2, true ) +
	           [ "share-link" => __( 'Share Link', 'Protect-MyWP-Files' ) ] + [ "filesize" => __( 'File Size', 'Protect-MyWP-Files' ) ] + [ "download-limit" => __( 'Download limit', 'Protect-MyWP-Files' ) ] + [ "date-limit" => __( 'Date limit', 'Protect-MyWP-Files' ) ] +
	           array_slice( $columns, 2, count( $columns ) - 2, true );

	return $columns;
} );


add_action( 'manage_protect-mywp-file_posts_custom_column', function ( $column, $post_id ) {
	global $post;
	switch ( $column ) {
		case 'download-limit' :
			$limit = get_post_meta( $post->ID, 'captain_media_secure_download_limit', true );

			if ( $limit == '' || $limit == - 1 ) {
				$limit = __( 'Unlimited', 'Protect-MyWP-Files' );
			} elseif ( $limit == 0 ) {
				$limit = '<span class="captain-limit">' . __( 'Download expired', 'Protect-MyWP-Files' ) . '</span>';
			}
			echo $limit;
			break;
		case 'date-limit' :
			$date_limit = get_post_meta( $post->ID, 'captain_media_secure_expiration_date', true );

			if ( $date_limit == '' || $date_limit == 0 ) {
				$txt = __( 'Unlimited', 'Protect-MyWP-Files' );
			} else {
				$now = time();
				$txt = '<span class="captain-limit">' . __( 'Date expired', 'Protect-MyWP-Files' ) . '</span>';
				if ( $date_limit > $now ) {
					$date_format = apply_filters( 'captain_expiration_date_format', get_option( 'date_format' ) );
					$txt         = date_i18n( $date_format, $date_limit );
				}
			}
			echo $txt;
			break;
		case 'share-link' :
			$token = get_post_meta( $post->ID, 'captain_media_secure_id', true );
			echo '<textarea readonly>' . Tools\get_share_url( $token ) . '</textarea>';
			break;
		case 'filesize' :
			$uploads_dir = Tools\get_folder_uploads() . '/' . apply_filters( 'captain_secure_folder', 'protect-files' );
			$name        = get_post_meta( $post->ID, 'captain_media_secure_name', true );
			if ( file_exists( $uploads_dir . '/' . $name ) ) {
				echo size_format( filesize( $uploads_dir . '/' . $name ), 2 );
			} else {
				_e( 'The file was not found', 'Protect-MyWP-Files' );
			}
			break;
		default :
			break;
	}
}, 10, 2 );

/**
 * Modification du lien du post uniquement pour notre CPT
 * On va lui mettre le lien du partage
 */
add_filter( 'get_edit_post_link', function ( $link, $post_id, $context ) {
	$type = get_post_type( $post_id );

	if ( $type == 'protect-mywp-file' ) {
		$token = get_post_meta( $post_id, 'captain_media_secure_id', true );

		return Tools\get_share_url( $token );
	}

	return $link;
}, 99, 3 );


/**
 * Suppression de l'option edition multiple
 * On gardera juste la suppression multiple
 */
add_filter( 'bulk_actions-edit-protect-mywp-file', function ( $bulk_actions ) {

	unset( $bulk_actions[ 'edit' ] );

	return $bulk_actions;
} );


/**
 * Suppression de toutes les actions rapides
 */
add_filter( 'post_row_actions', function ( $actions, $post ) {
	global $current_screen;
	if ( $current_screen->post_type != 'protect-mywp-file' ) {
		return $actions;
	}

	if ( isset( $actions[ 'trash' ] ) ) {
		$return[ 'trash' ]        = $actions[ 'trash' ];
		$return[ 'captain-edit' ] = '<a href="#captain-secure-popin"><span class="dashicons dashicons-edit"></span>' . __( 'Edit', 'Protect-MyWP-Files' ) . '</a>';
		$return[ 'captain-copy' ] = '<a class="copy-clipboard">' . __( 'Copy to clipboard', 'Protect-MyWP-Files' ) . '</a>';
	}
	if ( isset( $actions[ 'untrash' ] ) ) {
		$return[ 'untrash' ] = $actions[ 'untrash' ];
	}

	/*
			echo '<pre>'.print_r($actions, 1).'</pre>';
			die(); */

	return $return;
}, 10, 2 );



add_action( 'before_delete_post', function ( $pid, $post ) {
	if ( $post->post_type == 'protect-mywp-file' ) {
		$file_name   = get_post_meta( $pid, 'captain_media_secure_name', true );
		$uploads_dir = Tools\get_folder_uploads() . '/' . apply_filters( 'captain_secure_folder', 'protect-files' );
		if ( file_exists( $uploads_dir . '/' . $file_name ) ) {
			unlink( $uploads_dir . '/' . $file_name );
		}
	}
}, 10, 2 );
