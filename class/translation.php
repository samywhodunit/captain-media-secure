<?php

namespace Protect_MyWP_Files\i18n;

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );


add_action( 'plugins_loaded', function () {
	load_plugin_textdomain( 'Protect-MyWP-Files', false, dirname( plugin_basename( MYWP_FILES_FILE ) ) . '/languages/' );
}, 0 );
