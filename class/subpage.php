<?php

namespace Protect_MyWP_Files\Pages;

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

add_action( 'admin_menu', function () {

	add_submenu_page(
		'edit.php?post_type=protect-mywp-file',
		__( 'Protect-MyWP-Files settings', 'Protect-MyWP-Files' ),
		__( 'Settings', 'Protect-MyWP-Files' ),
		'manage_options',
		'protect-mywp-file-settings',
		function () {
			ob_start();
			include( MYWP_FILES_PATH . '/templates/protect-mywp-file-settings.php' );
			echo ob_get_clean();
		}

	);
} );