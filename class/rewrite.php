<?php

namespace Protect_MyWP_Files\Rewrite;

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

add_action( 'init', function () {

	$rewrite_share = apply_filters( 'captain_secure_rewrite_share', 'private-download' );

	add_rewrite_rule( $rewrite_share . '/([a-z0-9-]+)/?$', 'index.php?mywp-file=$matches[1]', 'top' );
	add_rewrite_tag( '%mywp-file%', '[^&]+' );


	// Flush rewrites
	flush_rewrite_rules();
}, 10, 0 );


add_filter( 'query_vars', function ( $qvars ) {
	$qvars[] = 'mywp-file';

	return $qvars;
} );
