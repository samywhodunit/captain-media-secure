<?php

namespace Protect_MyWP_Files\Tools;

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function get_folder_uploads() {
	if ( function_exists( 'wp_upload_dir' ) ) {
		$upload_dir = wp_upload_dir();
		if ( isset( $upload_dir[ 'basedir' ] ) ) {
			return $upload_dir[ 'basedir' ];
		}
	}

	return WP_CONTENT_DIR . '/uploads';
}

function check_env() {
	$soft = strtolower( $_SERVER[ 'SERVER_SOFTWARE' ] );
	if ( strpos( $soft, 'nginx' ) !== false ) {
		return 'nginx';
	}
	if ( strpos( $soft, 'apache' ) !== false ) {
		return 'apache';
	}

	return '';
}

function check_folder_create() {

	$uploads_dir = get_folder_uploads() . '/' . apply_filters( 'captain_secure_folder', 'protect-files' );

	if ( ! is_dir( $uploads_dir ) ) {
		if ( ! mkdir( $uploads_dir, 0755, true ) && ! is_dir( $uploads_dir ) ) {
			return sprintf( 'Directory "%s" was not created. Please do it manually for the proper functioning of the extension. The folder can have the rights 755 or 750.', $uploads_dir );
		}
	}

	return '';
}


function check_htaccess_create() {
	$uploads_dir = get_folder_uploads() . '/' . apply_filters( 'captain_secure_folder', 'protect-files' );

	if ( ! file_exists( $uploads_dir . '/.htaccess' ) ) {
		if ( file_put_contents( $uploads_dir . '/.htaccess', 'deny from all' ) ) {
			if ( ! file_exists( $uploads_dir . '/captain-america-test-secure.png' ) ) {
				if ( ! copy( MYWP_FILES_PATH . '/img/captain-america-test-secure.png', $uploads_dir . '/captain-america-test-secure.png' ) ) {
					error_log( "CAPTAIN MEDIA SECURE # The file has not been copied (" . MYWP_FILES_PATH . "/img/captain-america-test-secure.png)\n" );
					captain_start_plugin( 0 );

					return false;
				}
			}

			captain_start_plugin( 1 );

			return true;
		}
	} else {
		$perms = substr( sprintf( '%o', fileperms( $uploads_dir . '/.htaccess' ) ), - 4 );

		if ( $perms == '0644' ) {
			captain_start_plugin( 1 );

			return true;
		}
	}

	captain_start_plugin( 0 );

	return false;

}

function get_share_url( $token ) {
	$rewrite_share = apply_filters( 'captain_secure_rewrite_share', 'private-download' );
	$url_front     = get_home_url() . '/' . $rewrite_share;

	return $url_front . '/' . $token;
}


function captain_start_plugin( $new_value = 0 ) {
	$option_name = 'captain-start-plugin';
	if ( get_option( $option_name ) !== false ) {

		// The option already exists, so update it.
		update_option( $option_name, $new_value );

	} else {

		add_option( $option_name, $new_value );
	}
}


function get_role_media_secure( $media_id ) {
	$media_roles = get_post_meta( $media_id, 'captain_media_secure_roles', [] );
	if ( isset( $media_roles[ 0 ] ) && count( $media_roles[ 0 ] ) > 0 ) {
		$media_roles = $media_roles[ 0 ];
	}

	return $media_roles;
}
/*

function get_roles() {

	global $wp_roles;
	$return = [];
	foreach ( $wp_roles->roles as $key => $r ) {
		$return[ $key ] = translate_user_role( $r[ 'name' ] );
	}

	return $return;

} */
