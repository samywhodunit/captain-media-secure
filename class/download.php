<?php

namespace Protect_MyWP_Files\Download;

use Protect_MyWP_Files\Tools;

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

add_action( 'parse_query', function ( $query ) {
	$id = '';
	if ( $query ) {
		$vars = $query->query_vars;
		if ( array_key_exists( 'mywp-file', $vars ) ) {
			$id = $query->query_vars[ 'mywp-file' ];
		}
	}

	if ( $id != '' ) {
		if ( defined( 'REST_REQUEST' ) && REST_REQUEST ) {
			return;
		}
		if ( ! is_user_logged_in() ) {
			do_action( 'mywp_files_guest_redirect' );
			wp_die( __( 'You are not authorized to view this file', 'Protect-MyWP-Files' ), '', [ 'response' => 404, 'link_url' => get_home_url(), 'link_text' => __( 'Back to Home', 'Protect-MyWP-Files' ) ] );
		}

		$valid = apply_filters( 'mywp_files_custom_check', true );

		if ( $valid == false ) {
			do_action( 'mywp_files_custom_check_redirect' );
			wp_die( __( 'You are not authorized to view this file', 'Protect-MyWP-Files' ), '', [ 'response' => 404, 'link_url' => get_home_url(), 'link_text' => __( 'Back to Home', 'Protect-MyWP-Files' ) ] );
		}

		global $wpdb;
		$find = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta INNER JOIN $wpdb->posts ON $wpdb->posts.id = $wpdb->postmeta.post_id WHERE post_status = 'publish' AND  meta_key = 'captain_media_secure_id' AND meta_value = %s", $id ) );

		if ( isset( $find ) && isset( $find->post_id ) ) {

			$media_roles = Tools\get_role_media_secure( $find->post_id );

			if ( is_array( $media_roles ) && count( $media_roles ) > 0 ) {

				$search_key = array_search( 'all', $media_roles );
				if ( $search_key === false ) {
					$user         = wp_get_current_user();
					$roles_user   = (array) $user->roles;
					$roles_access = false;
					foreach ( $roles_user as $name_role ) {
						if ( in_array( $name_role, $media_roles ) ) {
							$roles_access = true;
							break;
						}
					}

					if ( $roles_access == false ) {
						do_action( 'mywp_files_user_role_redirect' );
						wp_die( __( 'You are not authorized to view this file', 'Protect-MyWP-Files' ), '', [ 'response' => 404, 'link_url' => get_home_url(), 'link_text' => __( 'Back to Home', 'Protect-MyWP-Files' ) ] );
					}
				}
			}

			$file_name   = get_post_meta( $find->post_id, 'captain_media_secure_name', true );
			$uploads_dir = Tools\get_folder_uploads() . '/' . apply_filters( 'captain_secure_folder', 'protect-files' );
			$file_secure = $uploads_dir . '/' . $file_name;
//

			if ( ! is_file( $file_secure ) ) {
				do_action( 'mywp_files_not_exist' );
				wp_die( __( 'The file does not exists.', 'Protect-MyWP-Files' ), '', [ 'response' => 404, 'link_url' => get_home_url(), 'link_text' => __( 'Back to Home', 'Protect-MyWP-Files' ) ] );
			}

			$limit = get_post_meta( $find->post_id, 'captain_media_secure_download_limit', true );

			if ( $limit != '' && $limit == 0 ) {
				do_action( 'mywp_files_download_limit' );
				wp_die( __( 'The file does not exists.', 'Protect-MyWP-Files' ), '', [ 'response' => 404, 'link_url' => get_home_url(), 'link_text' => __( 'Back to Home', 'Protect-MyWP-Files' ) ] );
			}

			$date_limit = get_post_meta( $find->post_id, 'captain_media_secure_expiration_date', true );
			$now        = time();

			if ( $date_limit != '' && $date_limit != 0 && $date_limit < $now ) {
				do_action( 'mywp_files_download_limit' );
				wp_die( __( 'The file does not exists.', 'Protect-MyWP-Files' ), '', [ 'response' => 404, 'link_url' => get_home_url(), 'link_text' => __( 'Back to Home', 'Protect-MyWP-Files' ) ] );
			}

			if ( $limit > 0 ) {
				$limit --;
			} else {
				$limit = - 1;
			}
			update_post_meta( $find->post_id, 'captain_media_secure_download_limit', $limit );

			$donwload_count = (int) get_post_meta( $find->post_id, 'captain_media_secure_donwload_count', true );

			$donwload_count ++;

			update_post_meta( $find->post_id, 'captain_media_secure_donwload_count', $donwload_count );

			header( 'Content-Description: Secure File' );
			header( 'Expires: 0' );
			header( 'Pragma: public' );
			header( 'Content-Type: application/octet-stream' );
			header( 'Content-Transfer-Encoding: binary' );
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Content-Disposition: attachment; filename="' . $file_name . '"' );
			header( 'Content-Length: ' . filesize( $file_secure ) );

			readfile( $file_secure );
			exit();
		}
	}

} );
