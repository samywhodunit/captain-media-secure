<?php

namespace Protect_MyWP_Files\Enqueue;

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

add_action( 'admin_enqueue_scripts', function ( $hook ) {

	if ( ! in_array( $hook, [ 'post.php', 'edit.php' ] ) ) {
		return;
	}
	if ( ! isset( $_GET[ 'post_type' ] ) || $_GET[ 'post_type' ] != 'protect-mywp-file' ) {
		return;
	}

	$start = get_option( 'captain-start-plugin' );

	if ( $start != 1 ) {
		return;
	}

	wp_register_style( 'magnific-popup-css', plugin_dir_url( MYWP_FILES_FILE ) . 'dist/magnific-popup.css', false );
	wp_enqueue_style( 'magnific-popup-css' );

	wp_register_style( 'mywp-files-css', plugin_dir_url( MYWP_FILES_FILE ) . 'css/mywp-files.css', false );
	wp_enqueue_style( 'mywp-files-css' );

	wp_enqueue_style( 'jquery-ui-datepicker-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css' );


	$max_upload_size = wp_max_upload_size();
	if ( ! $max_upload_size ) {
		$max_upload_size = 0;
	}
	$max_upload = sprintf(
	/* translators: %s: Maximum allowed file size. */
		__( 'Maximum upload file size: %s.' ),
		esc_html( size_format( $max_upload_size ) )
	);
	wp_enqueue_script( 'jquery-ui-core' );
	wp_enqueue_script( 'jquery-ui-datepicker' );

	wp_enqueue_script( 'jquery.magnific-popup-js', plugin_dir_url( MYWP_FILES_FILE ) . 'dist/jquery.magnific-popup.min.js', [], MYWP_FILES_VERSION );
	wp_enqueue_script( 'captain-validate', plugin_dir_url( MYWP_FILES_FILE ) . 'js/jquery.validate.min.js', [], MYWP_FILES_VERSION );
	wp_enqueue_script( 'captain-validate-additional', plugin_dir_url( MYWP_FILES_FILE ) . 'js/additional-methods.min.js', [], MYWP_FILES_VERSION );
	wp_enqueue_script( 'mywp-files-js', plugin_dir_url( MYWP_FILES_FILE ) . 'js/mywp-files.js', [], MYWP_FILES_VERSION );

	$captain_translation_ajax = [
		'invalid_filetype'        => __( 'Sorry, this file type is not permitted for security reasons.', 'Protect-MyWP-Files' ),
		'file_exceeds_size_limit' => __( '%s exceeds the maximum upload size for this site.', 'Protect-MyWP-Files' ),
		'unknownRequestFail'      => __( 'Looks like something&#8217;s gone wrong. Wait a couple seconds, and then try again.', 'Protect-MyWP-Files' ),
		'upload_success'          => __( 'The media has been correctly protected.', 'Protect-MyWP-Files' ),
		'add_media'               => __( 'Add a private File', 'Protect-MyWP-Files' ),
		'clipboard'               => __( 'Copy to clipboard', 'Protect-MyWP-Files' ),
		'dragdrop'                => __( 'Drag and drop file', 'Protect-MyWP-Files' ),
		'upload_file'             => __( 'Uploaded File', 'Protect-MyWP-Files' ),
		'remove'                  => __( 'Remove File', 'Protect-MyWP-Files' ),
		'start_upload'            => __( 'Start upload', 'Protect-MyWP-Files' ),
		'select_file'             => __( 'Select File', 'Protect-MyWP-Files' ),
	];

	$allowTypes = get_allowed_mime_types();

	wp_localize_script( 'mywp-files-js', 'CAPTAIN_INFO', [
		'ajax'              => admin_url( 'admin-ajax.php' ),
		'loader'            => esc_url( get_admin_url() . 'images/spinner-2x.gif' ),
		'nonce'             => wp_create_nonce( 'captain_upload_secure_file' ),
		'max_upload_format' => $max_upload,
		'max_upload'        => $max_upload_size,
		'allowTypes'        => implode( '|', array_keys( $allowTypes ) ),
		'i18n'              => $captain_translation_ajax

	] );

} );
